import os
import sys
import logging

# logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

import sentry_sdk
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_helper import AddTracebackDetailsIntegration
from vault_client import VaultClient
from dotenv import find_dotenv
from dotenv import load_dotenv

SERVICE_NAME = os.environ.get("SERVICE_NAME", "relay_postgres_changes")


# vvvvvvvvvv   VAULT    vvvvvvvvvv

def create_vars_from_locals(dict):
    module = sys.modules[__name__]
    for name, value in dict.items():
        if name.startswith("__") or name in ["base", "sys"]:
            continue
        # print(name)
        setattr(module, name, value)


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


base = {}
local_config = {}

dotenv_path = find_dotenv(usecwd=True)
if dotenv_path:
    load_dotenv(dotenv_path=dotenv_path)  # cwd - works better in celery
else:
    load_dotenv()

try:
    vault = VaultClient(SERVICE_NAME)
    base = dotdict(vault.get_dict("base"))
    local_config = dotdict(vault.get_local_config())
    # local_config.DB_TABLES['conversion_data'] = ['appsflyer_postback']

    logging.debug("Vault config ok")

except Exception as exc:
    logging.debug("Cannot configure vault %s: %s", exc.__class__, exc)

# ^^^^^^^^^^ VAULT ^^^^^^^^^^
# vvvvvvvvvv LOCAL vvvvvvvvvv


try:
    import traceback
    import settings_local  # pylint: disable=wrong-import-position

    if hasattr(settings_local, "base"):
        base.update(vars(settings_local.base))
        base = dotdict(base)
        # print(base)
    else:
        print("BASE NOT IMPORTED")
    local_config.update(vars(settings_local))
    local_config = dotdict(local_config)
    logging.debug("Imported settings_local.py")
except ImportError as e:
    traceback.print_exc()
    logging.debug("No settings_local.py provided : {}".format(e))

# ^^^^^^^^^^ LOCAL ^^^^^^^^^^

KAFKA = {
    "BOOTSTRAP_SERVERS": base["KAFKA"]["BOOTSTRAP_SERVERS"],
    "AUTH": base["KAFKA"].get("AUTH") or {}
}

USER_PATH = local_config.get("USER_PATH") or base.get("USER_PATH") or os.getenv("HOME") + "/"
LOG_ROOT = os.path.join(USER_PATH, "log/json")

sentry_sdk.init(
    dsn=local_config["SENTRY"]["DSN"],
    integrations=[RedisIntegration(), AddTracebackDetailsIntegration()],
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)
WATCHDOG_URL = base["WATCHDOG_URL"]
PROJECT_NAME = 'relay_postgres_changes'

create_vars_from_locals(local_config)
