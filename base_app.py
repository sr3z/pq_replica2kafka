"""Module to consume postgres replica datasets and send to kafka"""
from __future__ import print_function
import sys
import json
import re
import time
import shlex
import logging
import traceback
import signal
from datetime import date, datetime
from functools import wraps
from decimal import Decimal
from typing import Any
from typing import Optional
from typing import List
import psycopg2
import psycopg2.extras

from lazy_logging import logger_factory

from kafka.errors import KafkaTimeoutError
from kafka import KafkaProducer

try:
    import settings
except Exception:  # pylint: disable=broad-except
    traceback.print_exc()

# logger = logging.getLogger(__name__)
logger = logger_factory(
    'expertoption.relay_postgres_changes',
    is_private=True,
    level_stdout='INFO',
    log_root_path=settings.LOG_ROOT,
)
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

try:
    from watchdog_client import WatchdogClient
except Exception as err:  # pylint: disable=broad-except
    logger.error("IMPORT ERROR WATCHDOG: %s", err)

#
# POSTGRES TYPES
# https://www.highgo.ca/2020/03/24/logical-replication-between-postgresql-and-mongodb/
#
INT_TYPES = {
    "integer",
    "smallint",
    "bigint",
}

FLOAT_TYPES = {
    "float",
    "double precision",
    "real",
    "serial"
}


def retry_on_exception(times: int = 0, exceptions: Optional[List[Exception]] = None):
    """Decorator which reruns function {times} times if exception from {exceptions} list occurred
    or any exception if not set
    @param times int of times to rerun function on exception occurred
    @param exceptions list of Exceptions to catch, will be treated as any exception on non set"""
    if exceptions is None:
        exceptions = [Exception]

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for _ in range(times + 1):
                last_exception = None
                try:
                    return func(*args, **kwargs)
                except (*exceptions,) as exc:
                    last_exception = exc

            raise last_exception

        return wrapper

    return decorator


class ComplexEncoder(json.JSONEncoder):
    """Class extends JSONEncoder to dump Decimal, datetime objects and decodes bytes"""

    def default(self, o: Any) -> Any:
        if isinstance(o, Decimal):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        if isinstance(o, date):
            return str(o)
        if isinstance(o, bytes):
            return o.decode("utf-8")
        logger.error("OBJECT TYPE UNKNOWN: %s", type(o))

        return json.JSONEncoder.default(self, o)


class PostgresConsumer():
    """
    Class to consume postgres datasets and send parsed data to kafka
    """

    # pylint: disable=too-many-instance-attributes
    POSTFIX = ""
    TOPIC_TEMPLATE = "table_changes_{}_{}{}"
    TABLES_TO_MONITOR = []
    CHECK_DATA_UPDATE_THRESHOLD = 100
    LOGGER = logger
    DATABASE = {}
    DATABASE_NAME = ""
    WATCHDOG_INTERVAL = 30
    _exception_on_callback_processing = None

    def __init__(self, parent=None):
        # pylint: disable=invalid-name
        if parent:
            self.DATABASE = parent.DATABASE
            self.DB_TABLES = parent.DB_TABLES
            self.DATABASE_NAME = parent.DATABASE_NAME
            self.POSTFIX = parent.POSTFIX

        self.TABLES_TO_MONITOR = self.DATABASE.get("tables_to_monitor", [])

        if not self.TABLES_TO_MONITOR:
            self.TABLES_TO_MONITOR = self.DB_TABLES.get(self.DATABASE_NAME, [])

        self._last_data_update_or_connect_ts = None
        self.create_kafka_producer()

        self._watchdog = WatchdogClient(
            service_name="relay_postgres_changes_{}".format(self.DATABASE_NAME),
            project_name=settings.PROJECT_NAME,
            url=settings.WATCHDOG_URL,
            logger=self.LOGGER
        )

    def create_kafka_producer(self) -> None:
        """Creates connection to Kafka"""
        self._producer = KafkaProducer(
            bootstrap_servers=settings.KAFKA["BOOTSTRAP_SERVERS"],
            security_protocol=settings.KAFKA["AUTH"].get("security_protocol") or "PLAINTEXT",
            sasl_mechanism=settings.KAFKA["AUTH"].get("sasl_mechanism"),
            sasl_plain_username=settings.KAFKA["AUTH"].get("sasl_plain_username"),
            sasl_plain_password=settings.KAFKA["AUTH"].get("sasl_plain_password"),
            max_request_size=102400,
            request_timeout_ms=60000
        )

    @retry_on_exception(times=3, exceptions=[KafkaTimeoutError])
    def process_event(self, message, data_start) -> None:
        # pylint: disable=too-many-return-statements
        """Processes received from postgresql server message if its
        rotate, update, insert or delete event
        @param event BinLogEvent of even to be processed"""
        # {"change":[
        #             {"kind":"insert","schema":"public","table":"test01",
        #              "columnnames":["id","msg","date","comment"],
        #              "columntypes":["bigint","text","timestamp with time zone","character varying"],
        #              "columnvalues":[5,"test","2021-03-09 14:34:36.886853+03","commentZero"]
        #              }
        #           ]
        # }
        # {"change":[
        #             {"kind":"delete","schema":"public","table":"test01",
        #              "oldkeys":{
        #                  "keynames":["id"],
        #                  "keytypes":["bigint"],
        #                  "keyvalues":[5]
        #              }
        #             }
        #           ]
        # }
        change = json.loads(message).get("change")
        if not change:
            return
        event_type = change[0].get("kind").lower()
        schema = change[0].get("schema")
        table_name = change[0].get("table")
        columnnames = change[0].get("columnnames")
        columnvalues = change[0].get("columnvalues")
        rows = {}
        if columnnames and columnvalues:
            for k, v in zip(columnnames, columnvalues):
                rows[k] = v
        print(f"ROWS: {rows}")
        if not rows:
            return

        if table_name not in self.TABLES_TO_MONITOR:
            self.LOGGER.debug("%s [%s] IGNORED", table_name, self.TABLES_TO_MONITOR)
            return

        event = {
            "event_type": event_type,
            "timestamp": 0,
            "table": table_name,
            "schema": schema,
            "primary_key": "id",
        }
        event["timestamp"] = data_start

        if event_type == "rotate":
            return

        if event_type in ["update"]:
            event["rows"] = [{"after_values": rows}]
            self.LOGGER.debug(f"Received {event_type} event {message}")  # pylint: disable=logging-fstring-interpolation
            self.process_update_rows_event(event)
            return

        if event_type in ["insert"]:
            event["rows"] = [{"values": rows}]
            self.LOGGER.debug(f"Received {event_type} event {message}")  # pylint: disable=logging-fstring-interpolation
            self.process_write_rows_event(event)
            return

        if event_type in ["delete"]:
            event["rows"] = [{"values": rows}]
            self.LOGGER.debug(f"Received delete event {message}")  # pylint: disable=logging-fstring-interpolation
            self.process_delete_rows_event(event)
            return

    def process_update_rows_event(self, event) -> None:
        """Prepares data for update event and sends it to kafka topic
        @param event UpdateRowsEvent which contains of all changed rows
        including previous and new values"""
        self.send_data_to_kafka(event)
        self.data_update_packet_received_or_connected()

    def process_write_rows_event(self, event) -> None:
        """Prepares data for insert event and sends it to kafka topic
        @param event WriteRowsEvent which conatins of all newly inserted
        rows including values of all fields"""
        self.send_data_to_kafka(event)
        self.data_update_packet_received_or_connected()

    def process_delete_rows_event(self, event) -> None:
        """Prepares data for delete event and sends it to kafka topic
        @param event DeleteRowsEvent which contails all removed rows with values"""
        self.send_data_to_kafka(event)
        self.data_update_packet_received_or_connected()

    def data_update_packet_received_or_connected(self) -> None:
        """Updates timestamp when last time data(update, insert, delete) even was received
        or connection timestamp"""
        self._last_data_update_or_connect_ts = datetime.now()

    def is_long_time_no_data(self) -> bool:
        """Checks if too long time there was no data comming
        @return True if threshold reached between current timestamp and last data event
            timestamp otherwize False"""
        if self._last_data_update_or_connect_ts is None:
            return False

        last_data_update_or_connect_sec_ago = \
            (datetime.now() - self._last_data_update_or_connect_ts).total_seconds()

        if last_data_update_or_connect_sec_ago > self.CHECK_DATA_UPDATE_THRESHOLD:
            return True

        return False

    def on_kafka_send_success(self, metadata, timestamp: int) -> None:  # pylint: disable=unused-argument
        """Callback called on success while sending to kafka topic"""
        self._watchdog.ok(self.WATCHDOG_INTERVAL)

    def on_kafka_send_error(self, exc: Exception, *args, **kwargs) -> None:
        """Callback called if error occurred during sending data into kafka topic"""
        self.LOGGER.error('on_kafka_send_error %s %s %s', exc, args, kwargs)
        self._exception_on_callback_processing = exc

    def send_data_to_kafka(self, data: dict) -> None:
        """Sends prepared data to Kafka topic(separate topic for each monitoring table)
        @param table str of monitoring table for which event been received
        @param data dict of additional data"""

        if data.get("event_type") == "rotate:":
            return
        self.LOGGER.debug(f"SEND_DATA: {data}")  # pylint: disable=logging-fstring-interpolation
        print(f"SEND_DATA: {data}")  # pylint: disable=logging-fstring-interpolation
        table_name = data.get("table")
        dumped = json.dumps(data, cls=ComplexEncoder)
        TOPIC = self.TOPIC_TEMPLATE.format(table_name, self.DATABASE_NAME, self.POSTFIX)  # pylint: disable=invalid-name
        task = self._producer.send(TOPIC, value=dumped.encode("utf-8"), partition=0)
        task.add_callback(
            self.on_kafka_send_success,
            timestamp=data.get("timestamp"),
        )
        task.add_errback(self.on_kafka_send_error, data=data)
        self.LOGGER.debug("SEND_DATA_DONE")
        print("SEND_DATA_DONE")

    def __call__(self, msg):
        print("+++ DELTA: {}".format(msg.payload))
        self.process_event(msg.payload, msg.data_start)
        self.LOGGER.debug("CALL_DONE ..")
        msg.cursor.send_feedback(flush_lsn=msg.data_start)


class PostgresqlRelayProcessor:
    """
    Class to connect to dB and consume replication datasets
    """
    POSTFIX = ""
    DATABASE = {}
    DATABASE_NAME = ""
    DB_TABLES = []
    WATCHDOG_INTERVAL = None
    conn = None
    cur = None
    slot_name = "postgres_consumer"

    def start_connection(self):
        """
        dB init connection loop, waits for dB to be a master

        """
        self.conn = psycopg2.connect("host={} port={} dbname={} user={} password={}".\
                format(
                    self.DATABASE.get("host"),
                    self.DATABASE.get("port", 5432),
                    self.DATABASE_NAME,
                    self.DATABASE.get("user"),
                    self.DATABASE.get("password"),
                ),
                connection_factory=psycopg2.extras.LogicalReplicationConnection)
        self.slot_name=f"postgres_consumer_{self.DATABASE_NAME}"
        self.cur = self.conn.cursor()
        started = False

        while not started:
            try:
                self.cur.start_replication(slot_name=self.slot_name, decode=True, status_interval=1)
                started = True
            except psycopg2.ProgrammingError:
                self.cur.create_replication_slot(self.slot_name, output_plugin="wal2json")
                self.cur.start_replication(slot_name=self.slot_name, decode=True, status_interval=1)
                started = True
            except Exception as err:  # pylint: disable=broad-except
                logger.error("START_REPLICATION ERROR: %s\n SLEEP 5 sec\n", err)
                time.sleep(5)
        return self.cur

    def run(self) -> None:
        """
        Consumer main loop
        """
        logger.info("Starting connection")
        self.cur = self.start_connection()
        logger.info("Starting kafka")
        postgres_consumer = PostgresConsumer(self)
        logger.info("Starting streaming")
        count = 0
        while True:
            try:
                self.cur.consume_stream(postgres_consumer)
            except psycopg2.DatabaseError as err:
                logger.error("CONSUME_ERROR: %s", err)
                count += 1
                if count > 10:
                    break
                self.cur = self.start_connection()
            except (KeyboardInterrupt, SystemExit):
                self.cur.send_feedback(force=True)
                logger.warning("STOPPING ..")
                self.cur.close()
                self.conn.close()
                logger.warning("SLEEPPING 10 sec")
                time.sleep(10)
                logger.warning("The slot \"%s\" still exists.", self.slot_name)
                logger.warning("The slot \"%s\" still exists. "
                        "Drop it with SELECT pg_drop_replication_slot(\"%s\"); "
                        "if no longer needed.", self.slot_name, self.slot_name)
                logger.warning("WARNING: Transaction logs will accumulate in pg_xlog "
                        "until the slot is dropped.")
                break


def sigterm_handler(_signo, _stack_frame):
    """ SIGTERM handler function"""
    raise SystemExit

if __name__ == "__main__":
    # pylint: disable=invalid-name
    signal.signal(signal.SIGTERM, sigterm_handler)
    if len(sys.argv) < 2:
        logger.error("DATABASE_SET & DB REQUIRED")
        sys.exit(1)

    if not sys.argv[1]:
        logger.error("DATABASE_SET IS EMPTY")
        sys.exit(1)
    dbs = sys.argv[1].split(":")

    if not dbs:
        logger.error("DATABASE_SET IS INCORRECT: %s", dbs)
        sys.exit(1)

    if not hasattr(settings, dbs[0]):
        logger.error("DATABASE_SET IS MISSING: %s", dbs[0])
        sys.exit(1)

    processor = PostgresqlRelayProcessor()
    dbset = getattr(settings, dbs[0])

    if len(dbs) > 1:
        processor.DATABASE_NAME = dbs[1]
    else:
        processor.DATABASE_NAME = "default"
    logger.warning("DB_NAME: %s", processor.DATABASE_NAME)

    if len(dbs) > 2:
        processor.POSTFIX = dbs[2]
    else:
        processor.POSTFIX = ""

    processor.DATABASE = dbset.get(processor.DATABASE_NAME)

    if hasattr(settings, "DB_TABLES"):
        processor.DB_TABLES = getattr(settings, "DB_TABLES")
    else:
        processor.DB_TABLES = []

    processor.run()
