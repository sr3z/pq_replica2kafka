import sys
sys.path.append("/home/expert/app/config/")
import base

DATABASES = {
    "spaceone": {
        "host": "192.168.31.153",
        "name": "spaceone",
        "user": "postgres",
        "password": "postgres",
        "port": "5432",
        "tables_to_monitor": ["test01"],
    },
}
SENTRY = {
    "DSN": ""
}

