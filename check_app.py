from __future__ import print_function
import sys
import re
import time
import logging
import traceback

from lazy_logging import logger_factory

from kafka import KafkaConsumer

try:
    import settings
except Exception:
    traceback.print_exc()

# logger = logging.getLogger(__name__)
logger = logger_factory(
    "expertoption.relay_postgres_changes",
    is_private=True,
    level_stdout="DEBUG",
    log_root_path=settings.LOG_ROOT,
)
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)


def missing_elements(L):
    if not L:
        return None
    L = sorted(L)
    start, end = L[0], L[-1]
    ret = set(range(start, end + 1))
    if ret:
        logger.info("SET MIN: {} MAX: {}".format(min(ret), max(ret)))
    return ret.difference(L)


class KafkaProcessor:
    DATABASE = {}
    DATABASE_NAME = ""
    POSTFIX = ""
    TOPIC_TEMPLATE = "table_changes_{}_{}{}"
    DB_TABLES = []
    MAX_MSG_COUNT = 500

    def kafka_consumer(self):
        if self.DB_TABLES and self.DB_TABLES.get(self.DATABASE_NAME, []):
            table_name = self.DB_TABLES.get(self.DATABASE_NAME, ["test"])[0]
        else:
            table_name = "test"
        TOPIC = self.TOPIC_TEMPLATE.format(table_name, self.DATABASE_NAME, self.POSTFIX)
        logger.info("KAFKA TOPIC: {}".format(TOPIC))
        self.consumer = KafkaConsumer(
                                TOPIC,
                                bootstrap_servers=settings.KAFKA["BOOTSTRAP_SERVERS"],
                                security_protocol=settings.KAFKA["AUTH"].get("security_protocol") or "PLAINTEXT",
                                sasl_mechanism=settings.KAFKA["AUTH"].get("sasl_mechanism"),
                                sasl_plain_username=settings.KAFKA["AUTH"].get("sasl_plain_username"),
                                sasl_plain_password=settings.KAFKA["AUTH"].get("sasl_plain_password"),
                                auto_offset_reset='earliest',
                                enable_auto_commit=True,
                                consumer_timeout_ms=5000,
                                )

    def run(self) -> None:
        self.kafka_consumer()
        start_time = time.time()
        results = []
        while True:
            count = 0
            for message in self.consumer:
                count += 1
                value = message.value
                logger.info("RECVD: {}".format(value))
                match = re.findall(b'"insert", "rows": \[{"values": {"id": (\d+),', value)
                if match:
                    try:
                        results.append(int(match[0]))
                    except Exception as e:
                        logger.error("PARSED_VALUE_ERROR: {}".format(e))
                    logger.warning(len(results))
            ret = missing_elements(results)
            if ret:
                logger.warning("TEST FAILED .. {}".format(ret))
            else:
                logger.warning("TEST PASSED .. {}".format(ret))
            spent = time.time() - start_time
            if spent > 600 or not results:
                break
            logger.info("DONE.\nSPENT: {}".format(spent))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        logger.error("DATABASE_SET & DB REQUIRED")
        sys.exit(1)
    if not sys.argv[1]:
        logger.error("DATABASE_SET IS EMPTY")
        sys.exit(1)
    dbs = sys.argv[1].split(":")
    if not dbs:
        logger.error("DATABASE_SET IS INCORRECT: {}".format(dbs))
        sys.exit(1)
    if not hasattr(settings, dbs[0]):
        logger.error("DATABASE_SET IS MISSING: {}".format(dbs[0]))
        sys.exit(1)
    processor = KafkaProcessor()
    dbset = getattr(settings, dbs[0])
    if len(dbs) > 1:
        processor.DATABASE_NAME = dbs[1]
    else:
        processor.DATABASE_NAME = "default"
    logger.warning(processor.DATABASE_NAME)

    if len(dbs) > 2:
        processor.POSTFIX = dbs[2]
    else:
        processor.POSTFIX = ""

    processor.DATABASE = dbset.get(processor.DATABASE_NAME)
    if hasattr(settings, "DB_TABLES"):
        processor.DB_TABLES = getattr(settings, "DB_TABLES")
    else:
        processor.DB_TABLES = []
    processor.run()
